package br.com.jjss.microservice.fornecedor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.jjss.microservice.fornecedor.model.InfoFornecedor;
import br.com.jjss.microservice.fornecedor.service.InfoService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/info")
@Slf4j
public class InfoController {
	
	@Autowired
	private InfoService infoService;
	
	@RequestMapping("/{estado}")
	public InfoFornecedor getInfoPorEstado(@PathVariable String estado) {
		log.info("Recebido pedido de info do fornecedor do estado {}", estado);
		return infoService.getInfoPorEstado(estado);
	}
}
