package br.com.jjss.microservice.fornecedor.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReservaDTO {

	public Integer idReserva;
	
	public Integer tempoDePreparo;

}
