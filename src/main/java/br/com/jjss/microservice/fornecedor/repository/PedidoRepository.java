package br.com.jjss.microservice.fornecedor.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.jjss.microservice.fornecedor.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Long>{

}
