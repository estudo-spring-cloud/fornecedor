package br.com.jjss.microservice.fornecedor.model;

public enum PedidoStatus {
	RECEBIDO, PRONTO, ENVIADO;
}
